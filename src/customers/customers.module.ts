import { Module } from '@nestjs/common';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { RoleGuard } from '../app/guards/role.guard';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [],
  controllers: [CustomersController],
  providers: [
      CustomersService,
      {
          provide: APP_GUARD,
          useClass: RoleGuard,
      },
  ],
})

export class CustomersModule {}
