import {Controller, Get, Post, Param, Body} from '@nestjs/common';
import {CustomersService} from './customers.service';
import {Roles} from '../app/decorators/roles.decorator';

@Controller('customers')
export class CustomersController {
    constructor(private readonly customersService: CustomersService) {}

    @Get()
    @Roles('manager', 'admin')
    getAll(): Array<object> {
        return this.customersService.getAll();
    }

    @Get(":id")
    @Roles('manager', 'admin')
    getById(@Param('id') id): object {
        return this.customersService.getById(id);
    }

    @Post()
    @Roles('admin')
    create(@Body() customerDto: object) {  // Can use Dto like a type
        return this.customersService.create(customerDto);
    }
}
