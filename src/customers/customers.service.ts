import {Injectable} from '@nestjs/common';

@Injectable()
export class CustomersService {

    private customers: Array<object> = [];

    getAll(): Array<object> {
        return this.customers;
    }

    getById(id: number): object {
        return {};
    }

    create(customer: object): object {
        return {};
    }
}
