import { Module } from '@nestjs/common';
import { CustomersModule } from './customers/customers.module';
import { NewsModule } from './news/news.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
      ConfigModule.forRoot(),
      CustomersModule,
      NewsModule
  ],
})

export class AppModule {}
