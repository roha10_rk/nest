import {Controller, Get, Param} from '@nestjs/common';

@Controller('news')
export class NewsController {

    @Get()
    getAll(): Array<object> {
        return [];
    }

    @Get(":id")
    getById(@Param('id') id): object {
        return {}
    }
}
