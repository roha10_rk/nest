import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';

describe('NewsController', () => {
  let newsController: NewsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [],
    }).compile();

      newsController = app.get<NewsController>(NewsController);
  });

  describe('root', () => {
    it('should return empty array', () => {
      expect(newsController.getAll()).toEqual([]);
    });
  });
});
