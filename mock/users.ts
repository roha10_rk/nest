
export const users = {
    admin: {
        email: "manager@email.com",
        role: "admin"
    },
    manager: {
        email: "manager@email.com",
        role: "manager"
    }
};
